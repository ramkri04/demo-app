
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TouchableOpacity, ImageBackground, Dimensions, TextInput, Image, Button} from 'react-native';
import {Root} from 'native-base';
import Login from './src/Login/Login';
import Home from './src/Login/Home';
import ScreenOne from './src/screenTest';
import {authenticateUser} from './src/Api/LoginService';
import View from 'react-native-view'
import { Provider, SDRClient } from 'react-native-sdr';


const backgroundImage = require("./src/Images/background.png");
const logoImage = require("./src/Images/Logo.png");

const ApiClient = {
  method: "get",
  baseUrl: "http://192.168.0.101:3000",
  sdrTypes: {
    "Text": Text,
    "View": View,
    "Image": Image,
    "Button": TouchableOpacity,
	"TextInput": TextInput,
  }
}

export default class App extends Component<Props> {


    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            password: '',
            isLoggedIn: false,
            loginModule:''
        };
        this.checkLoggedIn = this.checkLoggedIn.bind(this);
    }

    checkLoggedIn(){
        this.setState({
            isLoggedIn: true
        })
    }
   
    componentWillMount(){
	    
	}

  render(){
	  console.log("LOGGING**********");
      // if(!this.state.isLoggedIn) {
          return (
           // <Root>
		 
				   //<ImageBackground style={styles.container} source={backgroundImage}>
                  // <Image source={logoImage} style={styles.logoImageStyle}/>
                 //<Login loginHandler={this.checkLoggedIn} />
                // </ImageBackground>
               //</Root>
				<Provider client={ApiClient}>
			   <View flex>
				<SDRClient
				  url="/login"
				{...{}}
				/>
				
			  </View>
			</Provider>
          );
      /*}
      else {
          return (
              <Root>
                <Home/>
              </Root>
          );
      }*/
   }
}

const styles = StyleSheet.create({
    container: {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        resizeMode: 'contain',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    logoImageStyle: {
        marginTop: '10%',
        marginLeft: '8%',
        marginRight: '8%',
        resizeMode: 'center',
    }
});
