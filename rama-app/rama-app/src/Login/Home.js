

import React, {Component} from 'react';
import {Platform, StyleSheet, Text} from 'react-native';
import {Button, Label, Input, Container, Toast} from 'native-base';
export default class Home extends Component<Props> {

    render() {
        return (
            <Container style={{margin: 10,justifyContent:'center'}}>
                    <Text style={{
                        color: '#4568DC',
                        fontFamily: 'Roboto',
                        fontSize: 20,
                        fontWeight: 'bold',
                        left:'5%',
                        right:'5%'
                    }}>Hi, Welcome to the Ascend App </Text>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

});