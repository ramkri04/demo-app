
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ImageBackground, Dimensions, TextInput, Image} from 'react-native';
import {Content, Item, Button, Label, Input, Container, Toast} from 'native-base';
import {authenticateUser} from '../Api/LoginService';

export default class Login extends React.Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loginModule:''
        };
    }


    validateUser() {
        if (this.state.username === '' || this.state.username == undefined) {
            Toast.show({
                text: 'Please enter Username',
                position: 'bottom',
                buttonText: 'Okay',
                duration: 5000,
                type: 'danger',
            })
        }
        else if (this.state.password === '' || this.state.password == undefined) {
            Toast.show({
                text: 'Please enter Password',
                position: 'bottom',
                buttonText: 'Okay',
                duration: 5000,
                type: 'danger',
            })
        }
        else if (!this.isValidPassword(this.state.password) || this.state.password.length <= 8) {
            Toast.show({
                text: "Password should have minimum 8 characters with 1 numeric and 1 alphabet",
                position: 'bottom',
                buttonText: 'Okay',
                duration: 5000,
                type: 'danger',
            })
        }
        else {
            authenticateUser(this.state.username, this.state.password).then(response=>{
                console.log("Login Module Js =>"+response);
                this.setState({
                    loginModule:response
                });
            });
                this.props.loginHandler();

        }
    }

    isValidPassword(text) {
        let regex = /^(?=.*\d)(?=.*[a-zA-Z]).{6,}$/;
        if (regex.test(text) === true) {
            return true;
        } else {
            return false;
        }
    }

    render() {
        return (
            <Container style={{backgroundColor: 'transparent', margin: 10}}>
                <View style={styles.viewStyle}>
                    <Item floatingLabel style={styles.userNameItem}>
                        <Label style={styles.userNameLabel}>Username</Label>
                        <Input
                            underlineColorAndroid={'#372D36'}
                            value={this.state.username}
                            style={{color: '#FFFFFF', paddingLeft: 10, fontSize: 18, fontWeight: '800'}}
                            onChangeText={(username) => this.setState({username})}
                        />
                    </Item>
                    <Item floatingLabel last style={styles.passwordItem}>
                        <Label style={styles.passwordlabel}>Password</Label>
                        <Input
                            underlineColorAndroid={'#372D36'}
                            value={this.state.password}
                            secureTextEntry={true}
                            style={{color: '#FFFFFF', paddingLeft: 10, fontSize: 18, fontWeight: '800'}}
                            onChangeText={(password) => this.setState({password})}/>
                    </Item>
                    <Text style={styles.textPassword}>Forgot Password</Text>
                </View>
                <Button block style={styles.loginButton} onPress={
                    () => this.validateUser()
                }>
                    <Text style={{color: '#FFFFFF', fontSize: 18, fontFamily: 'Roboto'}}>LOGIN</Text>
                </Button>
                <Button block style={styles.signUpButton}>
                    <Text style={{color: '#FFFFFF', fontSize: 18, fontFamily: 'Roboto'}}>SIGN UP</Text>
                </Button>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    viewStyle: {
        marginTop: "20%",
        height: Dimensions.get('window').height / 3
    },
    userNameItem: {
        width: "95%",
        left: "5%",
        right: "5%"
    },
    passwordItem: {
        width: "95%",
        left: "5%",
        right: "5%",
        top: "5%"
    },
    passwordlabel: {
        color: '#FFFFFF',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        marginLeft: '-2%',
        fontSize: 16
    },
    userNameLabel: {
        color: '#FFFFFF',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        marginLeft: '3%',
        fontSize: 16
    },
    textPassword: {
        color: '#FFFFFF',
        fontFamily: 'Roboto',
        fontSize: 15,
        left: "65%",
        marginTop: "8%",
        fontWeight: 'bold'
    },
    loginButton: {
        backgroundColor: '#4568DC',
        marginTop: '3%'
    },
    signUpButton: {
        marginTop: '8%',
        backgroundColor: '#303631'
    }
});